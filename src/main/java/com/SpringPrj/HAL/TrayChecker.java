package com.SpringPrj.HAL;

import com.SpringPrj.dataAccess.Drink;
import com.SpringPrj.dataAccess.DrinksRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TrayChecker implements Runnable {
    @Autowired
    private DrinksRepo repo;

    private void scanTray(Drink drink) {
        //TODO by HAL requirements
        int cansInTray = drink.getQuantity();

        if (cansInTray != drink.getQuantity()) {
            drink.setQuantity(cansInTray);
            repo.save(drink);
        }
    }

    @Override
    public void run() {
        while (true) {
            Iterable<Drink> allDrinks = repo.findAll();
            for (Drink drink : allDrinks) {
                scanTray(drink);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
