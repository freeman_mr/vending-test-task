package com.SpringPrj.dataAccess;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DrinksRepo extends CrudRepository<Drink, Long> {
    List<Drink> findByName(String name);
}
