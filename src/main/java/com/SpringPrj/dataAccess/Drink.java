package com.SpringPrj.dataAccess;


import org.springframework.validation.annotation.Validated;

import javax.persistence.*;

@Validated
@Entity
public class Drink {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(unique = true)
    private String name;

    @Column(unique = true)
    private int trayId;

    private int quantity;

    public Drink() {
    }

    public Drink(String name, int quantity, int trayId) {
        this.quantity = quantity;
        this.name = name;
        this.trayId = trayId;
    }

    public String toString() {
        return String.format("Drink[ name='%s', quantity='%s']", this.name, this.quantity);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTrayId() {
        return trayId;
    }

    public void setTrayId(int trayId) {
        this.trayId = trayId;
    }
}
