package com.SpringPrj.REST;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Item with this name already exist.")
public class HttpConflictException extends RuntimeException {
}
