
package com.SpringPrj.REST;

import com.SpringPrj.dataAccess.Drink;
import com.SpringPrj.dataAccess.DrinksRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/api"})
public class APIController {

    @Autowired
    private DrinksRepo repo;

    @GetMapping("/drinks")
    Iterable<Drink> getAllDrinks() {
        return repo.findAll();
    }

    @PostMapping("/drinks")
    @ResponseStatus(HttpStatus.OK)
    void CreateNewDrink(@RequestParam(value = "name", required = true) String name, @RequestParam(value = "value", required = true) int value, @RequestParam(value = "trayId", required = true) int trayId) {
        try {
            repo.save(new Drink(name, value, trayId));
        } catch (DataIntegrityViolationException ex) {
            throw new HttpConflictException();
        }
    }

    @GetMapping("/drinks/{name}")
    Drink getOneDrink(@PathVariable String name) {
        return findOneDrink(name);
    }

    //Do we need possibility to update quantity from REST???
    @PatchMapping("/drinks/{name}")
    @ResponseStatus(HttpStatus.OK)
    void updateOneDrink(@PathVariable String name, @RequestParam(value = "value", required = true) int value) {
        Drink drink = findOneDrink(name);
        drink.setQuantity(value);
        repo.save(drink);
    }

    @DeleteMapping("/drinks/{name}")
    @ResponseStatus(HttpStatus.OK)
    void deleteDrink(@PathVariable String name) {
        Drink drink = findOneDrink(name);
        repo.delete(drink);
    }

    @DeleteMapping("/drinks")
    @ResponseStatus(HttpStatus.OK)
    void deleteDrinks() {
        repo.deleteAll();
    }

    Drink findOneDrink(String name) throws HttpNotFoundException {
        List<Drink> drinks = repo.findByName(name);
        if (drinks.size() == 0)
            throw new HttpNotFoundException();
        return drinks.get(0);
    }
}
