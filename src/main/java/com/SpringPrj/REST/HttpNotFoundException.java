package com.SpringPrj.REST;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Item with this name can not be found.")
public class HttpNotFoundException extends RuntimeException {
}
