
package com.SpringPrj;

import com.SpringPrj.HAL.TrayChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Application {

    @Autowired
    TrayChecker trayChecker;

    public Application() {
    }

    @PostConstruct
    public void servicesStarter() {
        Thread newThread = new Thread(trayChecker);
        newThread.setDaemon(true);
        newThread.start();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return (args) -> {
        };
    }
}
